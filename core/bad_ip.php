<?php


namespace bad_ip;

if ( ! defined( 'THEPATH' ) ) {
    define( 'THEPATH', dirname( __FILE__ ) . '/' );
}
if ( ! defined( 'BAD_IP_BASE_URL' ) ) {
    define( 'BAD_IP_BASE_URL', 'https://bad-ip.iridiumintel.com' );
}
if ( ! defined( 'BAD_IP_JAIL_URL' ) ) {
    define( 'BAD_IP_JAIL_URL', 'https://bad-ip.iridiumintel.com/jail' );
}

class bad_ip
{


    public static $version = '0.0.2';
    public $settings = [
        'token' => 'bad_ip_test',
        'bot_access' => true,
        'deny_access' => true,
        'tor_block' => true,
        'bad_queries' => true,
        'login_incidents' => true,
        'login_attempts' => 3,
        'origin' => true,
        'reporter' => false,
        'log' => true,
    ];


    /**
     * bad_ip constructor.
     */
    public function __construct() {

    }


    /**
     * @codeCoverageIgnore
     */
    public function init() {
        self::headHook();
        self::_404Hook();
        if ( !defined('BAD_IP_LOADED') ) {
            define('BAD_IP_LOADED', true);
        }
    }

    /**
     * @return mixed
     */
    private function getUserIP() {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        isset($_SERVER['HTTP_CLIENT_IP']) ? $client  = $_SERVER['HTTP_CLIENT_IP'] : $client = null;
        isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $forward = $_SERVER['HTTP_X_FORWARDED_FOR'] : $forward = null;
        isset($_SERVER['REMOTE_ADDR']) ? $remote  = $_SERVER['REMOTE_ADDR'] : $remote = null;

        if(isset($client) && filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        }
        elseif(isset($forward) && filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        }
        else {
            $ip = $remote;
        }

        return $ip;
    }

    /**
     * @return bool
     */
    private function is404() {
        if (!self::url_exists(self::baseUrl(true))) {
            return true;
        }
        return http_response_code() == 404 ? true : false;
    }


    private function headHook() {
        $user_ip = self::getUserIP();
        $_now = time();

        if (self::checkIPInBlacklist($user_ip) && !self::checkIPInWhiteList($user_ip)) {
            self::toJail();
        }

        $bad_ipSettings = $this->settings;

        if (self::checkOnline(BAD_IP_BASE_URL)) {

            $data = array( 'uid' => $bad_ipSettings['token'], 'ip' => $user_ip, );
            $bad_ipSettings['tor_block'] === true ? $data['tor'] = true : null;
            $response = self::httpPost( BAD_IP_BASE_URL.'/api/v1/bad_ip/check/?uid='.$bad_ipSettings['token'], $data );
            $_rsp = @json_decode($response);
            $_data = @json_encode($_rsp->data);

            $dataArray = json_decode( $_data, true );
            isset($dataArray['tor_data'][0]) ? $isTor = true : $isTor = false;

            $isBot = self::is_bot();

            if ($bad_ipSettings['deny_access'] === true) {

                if ($bad_ipSettings['bot_access'] === true && $isBot) { //skip if is bot and bots are allowed
                    return;
                }

                if (self::checkIPInWhiteList($user_ip)) { //skip if is whitelisted
                    return;
                }

                if (isset($_rsp) && $_rsp->code == 666) { // if recorded in bad_ip public database
                    self::toJail();
                } else { // check for bad_query against public database

                    isset($_SERVER['QUERY_STRING']) ? $QS = $_SERVER['QUERY_STRING'] : $QS = null;
                    if (isset($QS) && $QS != '') {
                        isset($_SERVER['REQUEST_URI']) ? $actual_link = $_SERVER['REQUEST_URI'] : $actual_link = null;
                        if ( self::checkQuery($actual_link) ) {
                            return;
                        } //if query is whitelisted, skip
                        $data = array( 'uid' => $bad_ipSettings['token'], 'query' => $actual_link, );
                        $response = self::httpPost( BAD_IP_BASE_URL.'/api/v1/bad_query/check/?uid='.$bad_ipSettings['token'], $data );
                        $_rsp = @json_decode($response);
                        $_data = @json_encode($_rsp->data);

                        if (isset($_rsp) && $_rsp->code == 666) { // if recorded in bad_ip public database
                            self::toJail();
                        }

                    }

                }
            }

        }


        if ($this->settings['log']) {
            //        ob_start();
            echo <<<EOF
        <script>
            console.log(`%c
        Protected by

        888                    888          d8b
        888                    888          Y8P
        888                    888
        88888b.   8888b.   .d88888          888 88888b.
        888 "88b     "88b d88" 888          888 888 "88b
        888  888 .d888888 888  888          888 888  888
        888 d88P 888  888 Y88b 888          888 888 d88P
        88888P"  "Y888888  "Y88888 88888888 888 88888P"
                                                888
                                                888
                                                888
        `, "font-family:monospace");
        </script>
EOF;
            //        ob_end_flush();
        }


    }


    private function _404Hook() {

        $bad_ipSettings = $this->settings;
        $isBot = self::is_bot();

        if (!is_null($bad_ipSettings) && !empty($bad_ipSettings) && $bad_ipSettings['bad_queries'] === true) {

            $_now = time();
            $user_ip = self::getUserIP();
            $bad_ipSettings['origin'] ? $origin = self::baseUrl() : $origin = 'anonymous';
            $reporter = self::baseUrl();
            isset($_SERVER['REQUEST_URI']) ? $_action = $_SERVER['REQUEST_URI'] : $_action = null;
            isset($_SERVER['QUERY_STRING']) ? $QS = $_SERVER['QUERY_STRING'] : $QS = null;
            if (self::is404()) {

                if ($bad_ipSettings['bot_access'] && $isBot) { //skip if is bot and bots are allowed
                    return;
                }

                if (isset($QS) && $QS != '' && !self::checkIPInWhiteList($user_ip)) {
                    isset($_SERVER['REQUEST_URI']) ? $actual_link = $_SERVER['REQUEST_URI'] : $actual_link = null;
                    if ( self::checkQuery($_action) ) {
                        return;
                    } //if query is whitelisted, skip
                    $data = array('uid' => $bad_ipSettings['token'], 'ip' => $user_ip, 'origin' => $origin, 'reporter' => $reporter, 'action' => $actual_link);
                    $response = self::httpPost(BAD_IP_BASE_URL . '/api/v1/bad_ip/?uid='.$bad_ipSettings['token'], $data);
                    $_rsp = @json_decode($response);
                    $_data = @json_encode($_rsp->data);

                    /**
                     * Write report to database
                     */
                }


            }
        }
    }


    public function loginHook($success = false) {
        $bad_ipSettings = $this->settings;

        if (!is_null($bad_ipSettings) && !empty($bad_ipSettings) && $bad_ipSettings['login_incidents']) {
            session_start();
            isset($_SESSION['login_count']) ? $_SESSION['login_count'] ++ : $_SESSION['login_count'] = 1;
            if (isset($_SESSION['login_count']) && $_SESSION['login_count'] >= (int) $bad_ipSettings['login_attempts']) {

                if (!$success) {

                    $_now = time();
                    $user_ip = self::getUserIP();
                    $bad_ipSettings['login_incidents'] ? $origin = self::baseUrl() : $origin = 'anonymous';
                    $reporter = self::baseUrl();
                    $_action = 'login';

                    /**
                     * Send report to bad_ip http api
                     */
                    $data = array( 'uid' => $bad_ipSettings['token'], 'ip' => $user_ip, 'origin' => $origin, 'reporter' => $reporter, 'action' => $_action );
                    $response = self::httpPost( BAD_IP_BASE_URL.'/api/v1/bad_ip/?uid='.$bad_ipSettings['token'], $data );
                    $_rsp = @json_decode($response->body);
                    $_data = @json_encode($_rsp->data);

                    /**
                     * Write report to database
                     */

                } else {
                    self::login_success();
                }

            }

        }

    }

    private function url_exists($url) {
        $fp = curl_init($url);
        return !$fp ? false : true;
    }


    private function login_success() {
        if (isset($_SESSION['login_count'])){
            session_start();
            unset($_SESSION['login_count']);
        }
    }


    private function console_log($output, $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
            ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }


    /**
     * checks if visitor is crawler bot
     */
    private function is_bot(){
        if(isset($_SERVER['HTTP_USER_AGENT'])) {
            return preg_match('/rambler|abacho|acoi|accona|aspseek|altavista|estyle|scrubby|lycos|geona|ia_archiver|alexa|sogou|skype|facebook|twitter|pinterest|linkedin|naver|bing|google|yahoo|duckduckgo|yandex|baidu|teoma|xing|java\/1.7.0_45|bot|crawl|slurp|spider|mediapartners|\sask\s|\saol\s/i', $_SERVER['HTTP_USER_AGENT']);
        }
        return false;
    }

    /**
     * @param $domain
     * @return bool
     */
    private function checkOnline($domain) {
        $curlInit = curl_init($domain);
        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($curlInit,CURLOPT_HEADER,true);
        curl_setopt($curlInit,CURLOPT_NOBODY,true);
        curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

        $response = curl_exec($curlInit);

        curl_close($curlInit);
        return $response ? true : false;
    }


    /**
     * @return array
     */
    private function getQueryWhitelist() {  // todo decouple and centralize ... refactor
        if (!isset($queryListArr)) {
            $queryListArr = [];
            return $queryListArr;
        } else {
            $queryListExit = [];
            foreach ($queryListArr as $query) {
                $queryListExit[] = $query->query;
            }
            return $queryListExit;
        }
    }

    /**
     * @param $query
     * @return bool
     */
    private function checkQuery($query) {
        $queryListArr = self::getQueryWhitelist();
        return true ? in_array($query, $queryListArr) : false;
    }

    /**
     * @return array
     */
    private function getIPWhitelist() {
        if (!isset($ipListArr)) {
            $ipListArr = [];
            return $ipListArr;
        } else {
            $ipListExit = [];
            foreach ($ipListArr as $ip) {
                $ipListExit[] = $ip->ip;
            }
            return $ipListExit;
        }
    }

    /**
     * @param $ip
     * @return bool
     */
    private function checkIPInWhiteList($ip) {
        $ipArr = self::getIPWhitelist();
        return true ? in_array($ip, $ipArr) : false;
    }

    /**
     * @return array
     */
    private function getIPBlacklist() {
        if (!isset($ipListArr)) {
            $ipListArr = [];
            return $ipListArr;
        } else {
            $ipListExit = [];
            foreach ($ipListArr as $ip) {
                $ipListExit[] = $ip->ip;
            }
            return $ipListExit;
        }
    }

    /**
     * @param $ip
     * @return bool
     */
    private function checkIPInBlacklist($ip) {
        $ipArr = self::getIPBlacklist();
        return true ? in_array($ip, $ipArr) : false;
    }

    /**
     * @param string $url
     * @param bool $permanent
     */
    private function toJail($url = BAD_IP_JAIL_URL, $permanent = false) {
        if(headers_sent()) {
            echo "<script>window.open('".$url."','_self');</script>";
        }
        else {
            self::redirect($url);
        }
    }

    /**
     * @param $url
     * @param bool $permanent
     */
    protected function redirect($url, $permanent = false) {
        header('Location: ' . $url, true, $permanent ? 301 : 302);
        exit();
    }


    private function httpPost($url, $data) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }


    private function baseUrl($whole = false){
        isset($_SERVER['HTTP_HOST']) ? $_host = $_SERVER['HTTP_HOST'] : $_host = null;
        isset($_SERVER['REQUEST_URI']) ? $_uri = $_SERVER['REQUEST_URI'] : $_uri = null;
        if(isset($_host)) {
            isset($_SERVER['HTTPS']) ? $_https = $_SERVER['HTTPS'] : $_https = null;
            $protocol = ($_https && $_https != "off") ? "https" : "http";
        }
        else {
            $protocol = 'http';
        }
        return $whole ? $protocol . "://" . $_host . $_uri : $_host;
    }
}