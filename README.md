bad_ip PHP
====

A [bad_ip PHP][tanglecookieweb] is released under the open
source [MIT-license][MIT-license].

bad_ip PHP library

Installation
------------

`$ composer require bad-ip/ba-ip-php dev-master`

Usage
-----
Just instantiate it at bottom of your `index` as shown in example
```php

$bad_ip = new \bad_ip\bad_ip();
$bad_ip->init();

```
Settings "container" is presented as simple array 
```php
$settings = [
        'token' => 'bad_ip_test',
        'bot_access' => true,
        'deny_access' => true,
        'tor_block' => true,
        'bad_queries' => true,
        'login_incidents' => true,
        'origin' => true,
        'reporter' => false,
        'log' => true
    ];
``` 
So you can change the settings before `init()` as shown in example
```php
$bad_ip = new \bad_ip\bad_ip();
$bad_ip->settings['log'] = false;
$bad_ip->settings['deny_access'] = false;
$bad_ip->settings['tor_block'] = false;
$bad_ip->init();
```
Library comes with `loginHook`, with simple logic behind it. You call hook on unsuccessful login attempts, it will check against `Settings['login_attempts']` for number of max allowed attempts, and if reached it will report the IP. Failed login counter is reset with `loginHook` call with parameter `true` like shown in example below
```php
    // part of login post logic presenting `loginHook` implementation example

    $rsp = $mp->webLogin($data, 'user');
    $bad_ip = new \bad_ip\bad_ip();
    if ($rsp) {
        if (isset($rsp['error'])) {
            $context['notifications'] = [$rsp['error']];
        } else {
            if (isset($rsp['token'])) {
                $bad_ip->loginHook(true); // login success
                $secops->redirect('/user/dashboard', false);
            }
        }
    } else {
        $bad_ip->loginHook(); // login failed
        $context['notifications'] = ['Error in login process'];
    }
```

Reporting issues
----------------

See our [Contributing to bad_ip PHP][contributing] guide.

Support
-------

Have a question? Want to chat? Run into a problem? See our [community][support]
page.

---


[tanglecookieweb]: https://bad-ip.iridiumintel.com
[MIT-license]: http://opensource.org/licenses/mit-license.php
[docs]: https://bad-ip.iridiumintel.com/docs
[support]: https://bad-ip.iridiumintel.com/community
[contributing]: https://gitlab.com/bad-ip.io/bad-ip-php/CONTRIBUTING.md